<?php

/**
 * Theme functions. Initializes the Vamtam Framework.
 *
 * @package  wpv
 */

require_once( 'vamtam/classes/framework.php' );

new WpvFramework( array(
	'name' => 'auto-repair',
	'slug' => 'auto-repair',
) );



// TODO remove next line when the editor is fully functional, to be packaged as a standalone module with no dependencies to the theme
define( 'VAMTAM_EDITOR_IN_THEME', true ); include_once THEME_DIR . 'vamtam-editor/editor.php';

// only for one page home demos
function wpv_onepage_menu_hrefs( $atts, $item, $args ) {
	if ( 'custom' === $item->type && 0 === strpos( $atts['href'], '/#' ) ) {
		$atts['href'] = $GLOBALS['wpv_inner_path'] . $atts['href'];
	}
	return $atts;
}

if ( ( $path = parse_url( get_home_url(), PHP_URL_PATH ) ) !== null ) {
	$GLOBALS['wpv_inner_path'] = untrailingslashit( $path );
	add_filter( 'nav_menu_link_attributes', 'wpv_onepage_menu_hrefs', 10, 3 );
}

remove_action( 'admin_head', 'jordy_meow_flattr', 1 );


function kleingeschriebeneURL () {
	if (preg_match('/[A-Z]/', $_SERVER['REQUEST_URI'])) {
		header('Location: ' . strtolower($_SERVER['REQUEST_URI']), TRUE, 301);
		exit();
	}
}
add_action('init', 'kleingeschriebeneURL');



function custom_code(){
        if ( is_page( 32373 )) {
	?>
<!-- Google Code for Vielen Dank Seite Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1057709151;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Li6KCIrruWYQ37it-AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1057709151/?label=Li6KCIrruWYQ37it-AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	<?php
        }
}
add_action('wp_footer', 'custom_code');


function my_custom_js() {
    echo "
<!-- Hotjar Tracking Code for http://www.autogaslevekusen.de -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:212742,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
";
}
// Add hook for front-end <head></head>
//add_action('wp_head', 'my_custom_js');




/* Die XMLRPC-Schnittstelle komplett abschalten */
add_filter( 'xmlrpc_enabled', '__return_false' );
/* Den HTTP-Header vom XMLRPC-Eintrag bereinigen */
add_filter( 'wp_headers', 'AH_remove_x_pingback' );
 function AH_remove_x_pingback( $headers )
 {
 unset( $headers['X-Pingback'] );
 return $headers;
 }
 

