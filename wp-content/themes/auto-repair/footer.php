<?php
/**
 * Footer template
 *
 * @package wpv
 * @subpackage auto-repair
 */
?>

<?php if ( ! defined( 'WPV_NO_PAGE_CONTENT' ) ) : ?>
					</div> <!-- .limit-wrapper -->

				</div><!-- / #main ( do not remove this comment ) -->

			</div><!-- #main-content -->

			<?php if ( ! is_page_template( 'page-blank.php' ) ) : ?>
				<footer class="main-footer">
					<?php if ( wpv_get_optionb( 'has-footer-sidebars' ) ) : ?>
						<div class="footer-sidebars-wrapper">

<div class="lead-footer">
<div class="limit-wrapper">
<div class="row">

<div class="wpv-grid grid-1-3">
	<img style="margin: none" src="http://www.autogasleverkusen.de/umbau/wp-content/uploads/2016/07/autogas-angebot-trans.png" alt="autogas-angebot" class="size-medium wp-image-37496">
</div>

<div class="wpv-grid grid-2-3">
	<div class="_form_11"></div><script src="https://autogasleverkusen.activehosted.com/f/embed.php?id=11" type="text/javascript" charset="utf-8"></script>
</div>

</div>
</div>
</div>
							<?php WpvTemplates::footer_sidebars(); ?>
						</div>
					<?php endif ?>
				</footer>

				<?php do_action( 'wpv_before_sub_footer' ) ?>

				<?php if ( wpv_get_option( 'credits' ) != '' ) : ?>
					<div class="copyrights">
						<div class="<?php echo esc_attr( wpv_get_option( 'full-width-header' ) ? '' : 'limit-wrapper' ) ?>">
							<div class="row">
								<?php echo do_shortcode( wpv_get_option( 'credits' ) ); // xss ok ?>
							</div>
						</div>
					</div>
				<?php endif ?>
			<?php endif ?>

		</div><!-- / .pane-wrapper -->

<?php endif // WPV_NO_PAGE_CONTENT ?>
	</div><!-- / .boxed-layout -->
</div><!-- / #page -->

<div id="wpv-overlay-search">
	<form action="<?php echo esc_url( home_url() ) ?>/" class="searchform" method="get" role="search" novalidate="">
		<input type="text" required="required" placeholder="<?php esc_attr_e( 'Search...', 'auto-repair' ) ?>" name="s" value="" />
		<button type="submit" class="icon theme"><?php wpv_icon( 'theme-search2' ) ?></button>
		<?php if ( defined( 'ICL_LANGUAGE_CODE' ) ) : ?>
			<input type="hidden" name="lang" value="<?php echo esc_attr( ICL_LANGUAGE_CODE ) ?>"/>
		<?php endif ?>
	</form>
</div>

<?php get_template_part( 'templates/side-buttons' ) ?>

<?php

$ancestors = get_post_ancestors($post);
 if   (in_array(7390,$ancestors) AND count(get_post_ancestors($post->ID)) == 2) {

echo '

<script type="text/javascript" src="/umbau/autogas/js/loopedslider.js" ></script>
<script type="text/javascript" src="/umbau/autogas/js/slimbox2.js"></script>
<script type="text/javascript" src="/umbau/autogas/js/jquery.galleriffic.js"></script>
<script type="text/javascript" src="/umbau/autogas/js/jquery.galleriffic.run.js"></script>


';

}
					
?>



<?php wp_footer(); ?>
<!-- W3TC-include-js-head -->
</body>
</html>
