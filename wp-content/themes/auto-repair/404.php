<?php
/**
 * 404 page template
 *
 * @package wpv
 * @subpackage auto-repair
 */

get_header(); ?>

<div class="clearfix">
	
      <h1 class="entry-title"><?php _e('Seite nicht gefunden'); ?></h1>              
<p>Bitte entschuldigen Sie, die von Ihnen aufgerufene Seite wurde leider nicht gefunden.
Kehren Sie bitte zur <a href="/">Startseite zur&uuml;ck.</a>

<h2 id="pages">Alle Seiten</h2>
<ul>
<?php
// Add pages you'd like to exclude in the exclude here
wp_list_pages(
  array(
    'exclude' => '',
    'title_li' => '',
  )
);
?>
</ul>

<h2 id="posts">Blogbeitr&auml;ge</h2>
<ul>
<?php
// Add categories you'd like to exclude in the exclude here
$cats = get_categories('exclude=');
foreach ($cats as $cat) {
  echo "<ul>";
  query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
  while(have_posts()) {
    the_post();
    $category = get_the_category();
    // Only display a post link once, even if it's in multiple categories
    if ($category[0]->cat_ID == $cat->cat_ID) {
      echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
    }
  }
  echo "</ul>";
  echo "</li>";
}
?>
</ul>


</div>

<?php get_footer(); ?>