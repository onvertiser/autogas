jQuery(document).ready(function() {
jQuery('div.navigation').css({'width' : '300px', 'float' : 'left'});
jQuery('div.content').css('display', 'block');

// Initially set opacity on thumbs and add
	    // additional styling for hover effect on thumbs
var onMouseOutOpacity = 0.67;
jQuery('#thumbs ul.thumbs li').css('opacity', onMouseOutOpacity)
	.hover(
        function () {
		jQuery(this).not('.selected').fadeTo('fast', 1.0);
	    	    }, 
	function () {
		jQuery(this).not('.selected').fadeTo('fast', onMouseOutOpacity);
		    }
		);


    var gallery = jQuery('#gallery').galleriffic('#thumbs', {
        delay:                  3000, // in milliseconds
        numThumbs:              20, // The number of thumbnails to show page
        preloadAhead:           40, // Set to -1 to preload all images
        enableTopPager:         false,
        enableBottomPager:      true,
        imageContainerSel:      '#slideshow', // The CSS selector for the element within which the main slideshow image should be rendered
        controlsContainerSel:   '', // The CSS selector for the element within which the slideshow controls should be rendered
        captionContainerSel:    '#caption', // The CSS selector for the element within which the captions should be rendered
        loadingContainerSel:    '', // The CSS selector for the element within which should be shown when an image is loading
        renderSSControls:       true, // Specifies whether the slideshow's Play and Pause links should be rendered
        renderNavControls:      true, // Specifies whether the slideshow's Next and Previous links should be rendered
        playLinkText:           'Play',
        pauseLinkText:          'Pause',
        prevLinkText:           'Previous',
        nextLinkText:           'Next',
        nextPageLinkText:       'Next &rsaquo;',
        prevPageLinkText:       '&lsaquo; Prev',
        enableHistory:          false, // Specifies whether the url's hash and the browser's history cache should update when the current slideshow image changes 
        autoStart:              false, // Specifies whether the slideshow should be playing or paused when the page first loads 
	onChange:               function(prevIndex, nextIndex) {
			jQuery('#thumbs ul.thumbs').children()
			    .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
			    .eq(nextIndex).fadeTo('fast', 1.0);
		    },
	onTransitionOut:        function(callback) {
			jQuery('#caption').fadeTo('fast', 0.0);
			jQuery('#slideshow').fadeTo('fast', 0.0, callback);
		    },
	onTransitionIn:         function() {
			jQuery('#slideshow').fadeTo('fast', 1.0);
			jQuery('#caption').fadeTo('fast', 1.0);
		    },
	onPageTransitionOut:    function(callback) {
			jQuery('#thumbs ul.thumbs').fadeTo('fast', 0.0, callback);
		    },
	onPageTransitionIn:     function() {
			jQuery('#thumbs ul.thumbs').fadeTo('fast', 1.0);
		    }
    });
});
