
var anfrage_marka;
var anfrage_marka_komunikat;

var anfrage_model;
var anfrage_model_komunikat;

var anfrage_pojemnosc;
var anfrage_pojemnosc_komunikat;

var anfrage_rok;
var anfrage_rok_komunikat;

var anfrage_moc;
var anfrage_moc_komunikat;

var anfrage_nazwisko;
var anfrage_nazwisko_komunikat;

var anfrage_telefon;
var anfrage_telefon_komunikat;

var anfrage_mail;
var anfrage_mail_komunikat;

var anfrage_id_oddzial;
var anfrage_id_oddzial_komunikat;

var form_marka_submit;
var form_anfrage_data_ok;
var form_byl_submit=1;

var form_page_name="/Sprawdz-cene";
var anfrage_autocheck;

var lang='de';

function form_pytanie_test_id_oddzial() {
		if (form_byl_submit) {
			form_pytanie_reset(2);
		if ($(anfrage_id_oddzial).find(":selected").attr('not_possible')) {
				$(anfrage_id_oddzial_komunikat).text($(anfrage_id_oddzial_komunikat).attr(lang));
		} else {
				$(anfrage_id_oddzial_komunikat).text("");
				form_pytanie_prepare_2();
		}
		 
		}
		
		form_pytanie_submit_blokada();
}

function make_focus(what) {
	$(what).animate({
	    "margin-left": '+=5'
	  }, 100).animate({
	    "margin-left": '-=5'
	  }, 100)
	  .focus();
}

function form_pytanie_prepare_2() {
//	$('#angebotsanfrage_form_level_2').show();
	$('#angebotsanfrage_form_level_2 > table input').attr('disabled',false);
	$('#angebotsanfrage_form_level_2 > table select').attr('disabled',false);
	$('#marka_sel').load(form_page_name+"/DB-MARKA",function() {
		$('#marka_sel :eq(0)').attr("selected","selected").trigger('change');
		make_focus(this);
	});
	
}

function form_pytanie_prepare_3() {
/*	if ($('#marka_sel').find(":selected").attr("manual")) {
		
		$('#model_sel').hide();
		$(anfrage_model).show();
		
	} else {
*/		$('#model_sel').load(form_page_name+"/DB-MODEL",{ 'marka': $(anfrage_marka).val() },function() {
			$('#model_sel :eq(0)').attr("selected","selected").trigger('change');
			if ($('#model_sel option').size()==1) {
				$('#model_sel').hide();
			} else {
				$('#model_sel').show();
			}
//			$('#angebotsanfrage_form_level_3').show();
			$('#angebotsanfrage_form_level_3 > table input').attr('disabled',false);
			$('#angebotsanfrage_form_level_3 > table select').attr('disabled',false);
			make_focus(this);
		});
//	}
	
}

function form_pytanie_prepare_4() {
	$('#rok_sel').load(form_page_name+"/DB-ROK",{ 
		'marka': $(anfrage_marka).val(),
		'model': $(anfrage_model).val()		
	},function() {
			$('#rok_sel :eq(0)').attr("selected","selected").trigger('change');	
			if ($('#rok_sel option').size()==1) {
				$('#rok_sel').hide();
			} else {
				$('#rok_sel').show();
			}	
			//$('#angebotsanfrage_form_level_4').show();
			$('#angebotsanfrage_form_level_4 > table input').attr('disabled',false);
			$('#angebotsanfrage_form_level_4 > table select').attr('disabled',false);
			make_focus(this);
	});
}


function form_pytanie_prepare_5() {
	$('#pojemnosc_sel').load(form_page_name+"/DB-POJEMNOSC",{ 
		'marka': $(anfrage_marka).val(),
		'model': $(anfrage_model).val(),
		'rok': $(anfrage_rok).val()
	},function() {
		$('#pojemnosc_sel :eq(0)').attr("selected","selected").trigger('change');	
		if ($('#pojemnosc_sel option').size()==1) {
			$('#pojemnosc_sel').hide();
		} else {
			$('#pojemnosc_sel').show();
		}
		//$('#angebotsanfrage_form_level_5').show();
		$('#angebotsanfrage_form_level_5 > table input').attr('disabled',false);
		$('#angebotsanfrage_form_level_5 > table select').attr('disabled',false);
		make_focus(this);
	});
}

function form_pytanie_prepare_6() {
	$('#moc_sel').load(form_page_name+"/DB-MOC",{ 
		'marka': $(anfrage_marka).val(),
		'model': $(anfrage_model).val(),
		'rok': $(anfrage_rok).val(),
		'pojemnosc': $(anfrage_pojemnosc).val(),
		'moc': $('#pojemnosc_sel').find(':selected').attr('value_moc')
	},function() {
			$('#moc_sel :eq(0)').attr("selected","selected").trigger('change');	

			if ($('#moc_sel option').size()==1) {
				$('#moc_sel').hide();
			} else {
				$('#moc_sel').show();
			}
			
			moc=$('#pojemnosc_sel').find(':selected').attr('value_moc');
			if (moc) {
				//try/catch specjalnie dla IE6 bo wywalał błąd
				try {
                    $('#moc_sel [value="'+moc+'"]').attr("selected","selected");
                    setTimeout("$('#moc_sel').trigger('change');",100);
				}
				catch(ex) {
					setTimeout("$('#moc_sel [value=\""+moc+"\"]').attr(\"selected\",\"selected\").trigger('change')",1);
				}
				
			}
			
			//$('#angebotsanfrage_form_level_6').show();
			$('#angebotsanfrage_form_level_6 > table input').attr('disabled',false);
			$('#angebotsanfrage_form_level_6 > table select').attr('disabled',false);
			make_focus(this);
	});
}


function form_pytanie_prepare_7() {
     if (lang == 'de') {
         $('#norma_spalin').load(form_page_name+"/DB-EMISSIION-STANDARDS");
     }
     if (lang == 'pl_de') {
         $('#norma_spalin').load(form_page_name+"/DB-EMISSIION-STANDARDS");
     }

	$('#angebotsanfrage_form_level_7').show();
	//$('#angebotsanfrage_form_level_7 input').attr('disabled',false);
	//$('#angebotsanfrage_form_level_7 select').attr('disabled',false);

}
/*
function form_pytanie_prepare_8() {
	$('#angebotsanfrage_form_level_8').show();
}
function form_pytanie_prepare_9() {
	$('#angebotsanfrage_form_level_9').show();
}
function form_pytanie_prepare_10() {
	$('#angebotsanfrage_form_level_10').show();
}
function form_pytanie_prepare_11() {
	$('#angebotsanfrage_form_level_11').show();
}
function form_pytanie_prepare_12() {
	$('#angebotsanfrage_form_level_12').show();
}
function form_pytanie_prepare_13() {
	$('#angebotsanfrage_form_level_13').show();
}
function form_pytanie_prepare_14() {
	$('#angebotsanfrage_form_level_14').show();
}
*/

function form_pytanie_test_marka() {
		//sprawdz czy zmieniła się treśc
		if ($(anfrage_marka).val()==$(anfrage_marka).attr("ostatni_test_przy")) {
			return true;
		} else {
			$(anfrage_marka).attr("ostatni_test_przy",$(anfrage_marka).val());
		}
		
		if (form_byl_submit) {
			form_pytanie_reset(3);
		if ($(anfrage_marka).val()) {
				$(anfrage_marka_komunikat).text("");	
				form_pytanie_prepare_3();
		} else {
				$(anfrage_marka_komunikat).text($(anfrage_marka_komunikat).attr(lang));
		} 
		}
		form_pytanie_submit_blokada();
}

function form_pytanie_test_model() {
	//sprawdz czy zmieniła się treśc
	if ($(anfrage_model).val()==$(anfrage_model).attr("ostatni_test_przy")) {
		return true;
	} else {
		$(anfrage_model).attr("ostatni_test_przy",$(anfrage_model).val());
	}

	
		if (form_byl_submit) {
			form_pytanie_reset(4);

		if ($(anfrage_model).val()) {
				$(anfrage_model_komunikat).text("");	
				form_pytanie_prepare_4();

		} else {
				$(anfrage_model_komunikat).text($(anfrage_model_komunikat).attr(lang));
		} 
		}
		form_pytanie_submit_blokada();
}

function form_pytanie_test_pojemnosc() {
	//sprawdz czy zmieniła się treśc
	if ($(anfrage_pojemnosc).val()==$(anfrage_pojemnosc).attr("ostatni_test_przy")) {
		return true;
	} else {
		$(anfrage_pojemnosc).attr("ostatni_test_przy",$(anfrage_pojemnosc).val());
	}

	
	if (form_byl_submit) {
			form_pytanie_reset(6);

		if ($(anfrage_pojemnosc).val().match(/^\d+$/)) {
				$(anfrage_pojemnosc_komunikat).text("");	
				form_pytanie_prepare_6();

		} else {
				$(anfrage_pojemnosc_komunikat).text($(anfrage_pojemnosc_komunikat).attr(lang));
		}
		} 
		form_pytanie_submit_blokada();
}


function form_pytanie_test_rok() {
	//sprawdz czy zmieniła się treśc
	if ($(anfrage_rok).val()==$(anfrage_rok).attr("ostatni_test_przy")) {
		return true;
	} else {
		$(anfrage_rok).attr("ostatni_test_przy",$(anfrage_rok).val());
	}
	
	if (form_byl_submit) {
			form_pytanie_reset(5);

		if ($(anfrage_rok).val().length==0) {
			$(anfrage_rok_komunikat).text($(anfrage_rok_komunikat).attr(lang+"_1"));		
		} else {
			if (! ($(anfrage_rok).val().match(/^\d+$/))) {
				$(anfrage_rok_komunikat).text($(anfrage_rok_komunikat).attr(lang+"_2"));		
			} else {
				if (! ($(anfrage_rok).val() > 1949)) {
					$(anfrage_rok_komunikat).text($(anfrage_rok_komunikat).attr(lang+"_3"));		
				}	else {
					$(anfrage_rok_komunikat).text("");
					form_pytanie_prepare_5();
				}
			}
		}
		}
		form_pytanie_submit_blokada();
}


function form_pytanie_test_moc() {
	//sprawdz czy zmieniła się treśc
	if ($(anfrage_moc).val()==$(anfrage_moc).attr("ostatni_test_przy")) {
		return true;
	} else {
		$(anfrage_moc).attr("ostatni_test_przy",$(anfrage_moc).val());
	}

	
	if (form_byl_submit) {
			form_pytanie_reset(7);
				
		if ($(anfrage_moc).val().length==0) {
			$(anfrage_moc_komunikat).text($(anfrage_moc_komunikat).attr(lang));		
		} else {
			if (! ($(anfrage_moc).val().match(/^\d+$/))) {
				$(anfrage_moc_komunikat).text($(anfrage_moc_komunikat).attr(lang));		
			} else {
					$(anfrage_moc_komunikat).text("");
					form_pytanie_prepare_7();

			}
		}

		}
		form_pytanie_submit_blokada();
}

function form_pytanie_test_nazwisko() {
	//sprawdz czy zmieniła się treśc
	if ($(anfrage_nazwisko).val()==$(anfrage_nazwisko).attr("ostatni_test_przy")) {
		return true;
	} else {
		$(anfrage_nazwisko).attr("ostatni_test_przy",$(anfrage_nazwisko).val());
	}
	
	if (form_byl_submit) {
						
		if ($(anfrage_nazwisko).val().length==0) {
			$(anfrage_nazwisko_komunikat).text($(anfrage_nazwisko_komunikat).attr(lang));		
		} else {
			$(anfrage_nazwisko_komunikat).text("");
		}

		}
		form_pytanie_submit_blokada();
}


function form_pytanie_na_pl_test_czy_de() {


    if (lang=='pl') {

        $('form[name="pytanie"]').removeAttr("action");

        var telefon_test=$(anfrage_telefon).val();
        if (telefon_test) {
            telefon_test=telefon_test.replace(/\D/gm,"");
        }

        //jeśli telefon wygląda na niemiecki to podmieniamy form
         if (telefon_test.match(/^(49|0049|015|016|017).*/)) {
                $('form[name="pytanie"]').attr("action",'/de/Angebotsanfrage');
        }

        //jeśli email koniec na de to podmieniamy formularz
        if ($(anfrage_mail).val().match(/^[0-9a-zA-Z_.+-]+@([0-9a-zA-Z-]+\.)+de$/)) {
                $('form[name="pytanie"]').attr("action",'/de/Angebotsanfrage');
        } 
   }
}



function form_pytanie_test_telefon() {
    //sprawdz czy zmieniła się treśc
//    if ($(anfrage_telefon).val()==$(anfrage_telefon).attr("ostatni_test_przy")) {
//        return true;
//    } else {
//       $(anfrage_telefon).attr("ostatni_test_przy",$(anfrage_telefon).val());
//    }
    
    var telefon_test=$(anfrage_telefon).val();
    if (telefon_test) {
        telefon_test=telefon_test.replace(/\D/gm,"");
    }
    
//    alert(telefon_test);
    
    if (form_byl_submit) {
        //lev
      if ($(anfrage_id_oddzial).find(":selected").val()==5)
      {
                        
        if (telefon_test.length<6) {
            $(anfrage_telefon_komunikat).text($(anfrage_telefon_komunikat).attr(lang));       
        } else {

            if (! (telefon_test.match(/^\d+$/))) {
                $(anfrage_telefon_komunikat).text($(anfrage_telefon_komunikat).attr(lang));       
             } else {
                if (telefon_test.match(/^000000/)) {
                      $(anfrage_telefon_komunikat).text($(anfrage_telefon_komunikat).attr(lang));       
                } else {
 
                   if (telefon_test.match(/^123456/)) {
                          $(anfrage_telefon_komunikat).text($(anfrage_telefon_komunikat).attr(lang));       
                    } else {
 
                         $(anfrage_telefon_komunikat).text("");
                    }
 
               }
             }
        }
      } else {
            $(anfrage_telefon_komunikat).text("");
      }
    }
        form_pytanie_submit_blokada();
}


function form_pytanie_test_mail() {
    //sprawdz czy zmieniła się treśc
    if ($(anfrage_mail).val()==$(anfrage_mail).attr("ostatni_test_przy")) {
        return true;
    } else {
        $(anfrage_mail).attr("ostatni_test_przy",$(anfrage_mail).val());
    }
    
    if (form_byl_submit) {
                        
        if ($(anfrage_mail).val().length==0) {
            $(anfrage_mail_komunikat).text($(anfrage_mail_komunikat).attr(lang));       
        } else {
            if (! ($(anfrage_mail).val().match(/^[0-9a-zA-Z_.+-]+@([0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/))) {
                $(anfrage_mail_komunikat).text($(anfrage_mail_komunikat).attr(lang));       
            } else {
                $(anfrage_mail_komunikat).text("");
            }
        }

        }
        form_pytanie_submit_blokada();
}


function form_pytanie_test_wszystko() {
	form_pytanie_test_id_oddzial();
	form_pytanie_test_marka();
	form_pytanie_test_model();
	form_pytanie_test_pojemnosc();
	form_pytanie_test_rok();
	form_pytanie_test_moc();
	form_pytanie_test_nazwisko();
    form_pytanie_test_telefon();
	form_pytanie_test_mail();
}

$(document).ready(function() {

	 lang=$('body').attr('lang');
     if (lang=='pl') {
        form_page_name="/Sprawdz-cene";
     }
     if (lang=='de') {
        form_page_name="/Angebotsanfrage";
     }
     if (lang=='pl_de') {
        form_page_name="/de/Angebotsanfrage";
         $('[de]').each(function() {
            $(this).attr('pl_de',$(this).attr('de'));
         });
     }
     
	
	 form_marka_submit=$('form[name="pytanie"] input[name="submit"]');

	 anfrage_id_oddzial=$('#id_oddzial');
	 anfrage_id_oddzial_komunikat=$('#id_oddzial_komunikat').hide();
	 $(anfrage_id_oddzial).change(form_pytanie_test_id_oddzial);
	
	 anfrage_marka=$('#marka');	 
	 anfrage_marka_komunikat=$('#marka_komunikat').hide();
	 $(anfrage_marka).keyup(form_pytanie_test_marka).focus(function() {
		 anfrage_autocheck=setInterval("form_pytanie_test_marka()",1000);
	 }).blur(function() {
		 clearInterval(anfrage_autocheck);
	 });
	 
	 $(anfrage_marka).hide();
	 
	 $('#marka_sel').change(function () {
		 if ($(this).find(":selected").attr('manual')) {
			 $(anfrage_marka).show();
			 $(anfrage_marka_komunikat).show();
		 } else {
			 $(anfrage_marka).hide();			 
			 $(anfrage_marka_komunikat).hide();			 
		 }
		 $(anfrage_marka).val($(this).find(":selected").val());
		 $(anfrage_marka).trigger('keyup');
	 });

	 
	 
	 
	 anfrage_model=$('#model');	 
	 anfrage_model_komunikat=$('#model_komunikat').hide();
	 $(anfrage_model).keyup(form_pytanie_test_model).focus(function() {
		 anfrage_autocheck=setInterval("form_pytanie_test_model()",1000);
	 }).blur(function() {
		 clearInterval(anfrage_autocheck);
	 });
	 $(anfrage_model).hide();

	 $('#model_sel').change(function () {
		 if ($(this).find(":selected").attr('manual')) {
			 $(anfrage_model).show();
			 $(anfrage_model_komunikat).show();
		 } else {
			 $(anfrage_model).hide();			 
			 $(anfrage_model_komunikat).hide();			 
		 }
		 $(anfrage_model).val($(this).find(":selected").val());
		 $(anfrage_model).trigger('keyup');
	 });
	 
	 
	 anfrage_pojemnosc=$('#pojemnosc');	 
	 anfrage_pojemnosc_komunikat=$('#pojemnosc_komunikat').hide();
	 $(anfrage_pojemnosc).keyup(form_pytanie_test_pojemnosc).focus(function() {
		 anfrage_autocheck=setInterval("form_pytanie_test_pojemnosc()",1000);
	 }).blur(function() {
		 clearInterval(anfrage_autocheck);
	 });
	 $(anfrage_pojemnosc).hide();

	 $('#pojemnosc_sel').change(function () {
		 if ($(this).find(":selected").attr('manual')) {
			 $(anfrage_pojemnosc).show();
			 $(anfrage_pojemnosc_komunikat).show();
		 } else {
			 $(anfrage_pojemnosc).hide();			 
			 $(anfrage_pojemnosc_komunikat).hide();
		 }
		 $(anfrage_pojemnosc).val($(this).find(":selected").val());
		 $(anfrage_pojemnosc).trigger('keyup');
	 });	 
	 

	 anfrage_moc=$('#moc');	 
	 anfrage_moc_komunikat=$('#moc_komunikat').hide();
	 $(anfrage_moc).keyup(form_pytanie_test_moc).focus(function() {
		 anfrage_autocheck=setInterval("form_pytanie_test_moc()",1000);
	 }).blur(function() {
		 clearInterval(anfrage_autocheck);
	 });
	 $(anfrage_moc).hide();

	 $('#moc_sel').change(function () {
		 if ($(this).find(":selected").attr('manual')) {
			 $(anfrage_moc).show();
			 $(anfrage_moc_komunikat).show();
		 } else {
			 $(anfrage_moc).hide();			 
			 $(anfrage_moc_komunikat).hide();			 
		 }
		 $(anfrage_moc).val($(this).find(":selected").val());
		 $(anfrage_moc).trigger('keyup');
	 });	

	 anfrage_rok=$('#rok');	 
	 anfrage_rok_komunikat=$('#rok_komunikat').hide();
	 $(anfrage_rok).keyup(form_pytanie_test_rok).focus(function() {
		 anfrage_autocheck=setInterval("form_pytanie_test_rok()",1000);
	 }).blur(function() {
		 clearInterval(anfrage_autocheck);
	 });
	 $(anfrage_rok).hide();

	 $('#rok_sel').change(function () {
		 if ($(this).find(":selected").attr('manual')) {
			 $(anfrage_rok).show();
			 $(anfrage_rok_komunikat).show();
		 } else {
			 $(anfrage_rok).hide();			 
			 $(anfrage_rok_komunikat).hide();			 
		 }
		 $(anfrage_rok).val($(this).find(":selected").val());
		 $(anfrage_rok).trigger('keyup');
	 });	
	 
	 anfrage_nazwisko=$('#nazwisko');	 
	 anfrage_nazwisko_komunikat=$('#nazwisko_komunikat');
//	 $(anfrage_nazwisko).keyup(form_pytanie_test_nazwisko).focus(function() {
//		 anfrage_autocheck=
		 setInterval("form_pytanie_test_nazwisko()",1000);
//	 }).blur(function() {
//		 clearInterval(anfrage_autocheck);
//	 });

     anfrage_telefon=$('#tel');    
     anfrage_telefon_komunikat=$('#tel_komunikat');
//   $(anfrage_nazwisko).keyup(form_pytanie_test_nazwisko).focus(function() {
//       anfrage_autocheck=
     setInterval("form_pytanie_test_telefon()",1000);
//   }).blur(function() {
//       clearInterval(anfrage_autocheck);
//   });


	 anfrage_mail=$('#mail');	 
	 anfrage_mail_komunikat=$('#mail_komunikat');
//	 $(anfrage_mail).keyup(form_pytanie_test_mail).focus(function() {
//		 anfrage_autocheck=
		 setInterval("form_pytanie_test_mail()",1000);
//	 }).blur(function() {
//		 clearInterval(anfrage_autocheck);
//	 });

     setInterval("form_pytanie_na_pl_test_czy_de()",1000);


	 $('form[name="pytanie"]').submit(function() {
	 		form_byl_submit=1;
	 		//form_pytanie_test_wszystko();
	 		if ($(form_anfrage_data_ok).attr("value")>0) {
				return true; 
			} else {
				return false;
			}	 			 
	 });

	 form_anfrage_data_ok=$('#form_anfrage_data_ok');

	 make_focus(anfrage_id_oddzial);
	 if (lang == 'pl') {
		change_form_to_pl();
	 }

     if (lang == 'pl_de') {
        change_form_to_pl_de();
     }

    if (lang =='de') {
        change_form_to_de();
    }

	 form_pytanie_test_wszystko();
	 
	 $('tr[class="szczegoly_wyceny"]').hide();
	 //po ogłoszeniu formularza
	 $('a[class="link_do_szczegolow"]').toggle(
			    function () { 
			    	$("#szczegoly_"+$(this).attr('numer')).show();
			    	return false; 
			    },
			    function () { 
			    	$("#szczegoly_"+$(this).attr('numer')).hide();
			    	return false; }
			 );
	 

}
);

function change_form_to_pl() {
	//ukryć oddział
	$('#id_oddzial :eq(1)').attr("selected","selected");
	$('#table_oddzial').hide();
	$('#table_plec').hide();
	$('#table_imie').hide();
	$('#table_norma').hide();
	$('#symbol').hide();
}

function change_form_to_pl_de() {
    //ukryć oddział
    $('#id_oddzial :eq(1)').attr("selected","selected");
    $('#table_oddzial').hide();
}

function change_form_to_de() {
    //ukryć oddział
    $('#id_oddzial :eq(1)').attr("selected","selected");
//    $('#table_oddzial').hide();
}

function form_pytanie_reset($i) {	
	if ($i<="2") {
//		$('#angebotsanfrage_form_level_2').hide();
		$('#angebotsanfrage_form_level_2 > table input').attr('disabled',true);
		$('#angebotsanfrage_form_level_2 > table select').attr('disabled',true);
		$('#marka').val('').trigger('keyup');
	}
	
	if ($i<="3") {
//		$('#angebotsanfrage_form_level_3').hide();
		$('#angebotsanfrage_form_level_3 > table input').attr('disabled',true);
		$('#angebotsanfrage_form_level_3 > table select').attr('disabled',true);
		$('#model').val('').trigger('keyup');
	}

	if ($i<="4") {
//		$('#angebotsanfrage_form_level_4').hide();
		$('#angebotsanfrage_form_level_4 > table input').attr('disabled',true);
		$('#angebotsanfrage_form_level_4 > table select').attr('disabled',true);
		$('#rok').val('').trigger('keyup');
	}

	if ($i<="5") {
//		$('#angebotsanfrage_form_level_5').hide();
		$('#angebotsanfrage_form_level_5 > table input').attr('disabled',true);
		$('#angebotsanfrage_form_level_5 > table select').attr('disabled',true);
		$('#pojemnosc').val('').trigger('keyup');
	}
	
	if ($i<="6") {
//		$('#angebotsanfrage_form_level_6').hide();
		$('#angebotsanfrage_form_level_6 > table input').attr('disabled',true);
		$('#angebotsanfrage_form_level_6 > table select').attr('disabled',true);
		$('#moc').val('').trigger('keyup');
	}
	
	if ($i<="7") {
		$('#angebotsanfrage_form_level_7').hide();
//		$('#angebotsanfrage_form_level_7 input').attr('disabled',true);
//		$('#angebotsanfrage_form_level_7 select').attr('disabled',true);
	}


}


function form_pytanie_submit_blokada() {
		if (
					$(anfrage_marka_komunikat).text() 
				+	$(anfrage_model_komunikat).text()
				+	$(anfrage_pojemnosc_komunikat).text()
				+	$(anfrage_rok_komunikat).text()					
				+	$(anfrage_moc_komunikat).text()					
				+	$(anfrage_nazwisko_komunikat).text()					
				+	$(anfrage_mail_komunikat).text()
                +   $(anfrage_telefon_komunikat).text()
				+ $(anfrage_id_oddzial).find(":selected").attr('not_possible')
				) 
		{
				$(form_marka_submit).attr("disabled","disabled");
				$(form_anfrage_data_ok).attr("value",'0');
		} else {
				$(form_marka_submit).removeAttr("disabled");		
				$(form_anfrage_data_ok).attr("value",'1');
		}
}
